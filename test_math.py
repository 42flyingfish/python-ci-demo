from basic_math import add_two, mul_two, div_two
import unittest


class TestMath(unittest.TestCase):
    def test_add_two(self):
        self.assertEqual(add_two(2, 2), 4)

    def test_mul_two(self):
        self.assertEqual(mul_two(2, 2), 4)

    def test_div_two(self):
        self.assertEqual(div_two(2, 2), 1)
